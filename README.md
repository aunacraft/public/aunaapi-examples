**AunaAPI usage**

Examples:
- [CoinAPIExample](https://gitlab.com/aunacraft/public/aunaapi-examples/-/blob/master/src/main/examples/CoinAPIExample.java)
- [DatabaseHandlerExample](https://gitlab.com/aunacraft/public/aunaapi-examples/-/blob/master/src/main/examples/DatabaseHandlerExample.java)
- [GuiExample](https://gitlab.com/aunacraft/public/aunaapi-examples/-/blob/master/src/main/examples/GuiExample.java)
- [ItemBuilderExample](https://gitlab.com/aunacraft/public/aunaapi-examples/-/blob/master/src/main/examples/ItemBuilderExample.java)
- [MessageServiceExample](https://gitlab.com/aunacraft/public/aunaapi-examples/-/blob/master/src/main/examples/MessageServiceExample.java)
- [NPCExample](https://gitlab.com/aunacraft/public/aunaapi-examples/-/blob/master/src/main/examples/NPCExample.java)
- [SignMenuExample](https://gitlab.com/aunacraft/public/aunaapi-examples/-/blob/master/src/main/examples/SignMenuExample.java)
- [ValidatorExample](https://gitlab.com/aunacraft/public/aunaapi-examples/-/blob/master/src/main/examples/ValidatorExample.java)
- [LocationUtilExample](https://gitlab.com/aunacraft/public/aunaapi-examples/-/blob/master/src/main/examples/LocationUtilExample.java)
- [AunaPlayerExample](https://gitlab.com/aunacraft/public/aunaapi-examples/-/blob/master/src/main/examples/AunaPlayerExample.java)
- [MapBuilderExample](https://gitlab.com/aunacraft/public/aunaapi-examples/-/blob/master/src/main/examples/MapBuilderExample.java)
- [CommandExample](https://gitlab.com/aunacraft/public/aunaapi-examples/-/blob/master/src/main/examples/CommandExample.java)