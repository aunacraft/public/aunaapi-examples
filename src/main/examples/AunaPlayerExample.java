import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.player.PlayerMeta;
import org.bukkit.entity.Player;

public class AunaPlayerExample {

    public void handle(Player player){

        AunaPlayer aunaPlayer = AunaAPI.getApi().getPlayer(player.getUniqueId()); // Gets the AunaPlayer with a UUID
        aunaPlayer = AunaAPI.getApi().getPlayer(player.getName()); // Gets the AunaPlayer with a Name

        Player bukkitPlayer = aunaPlayer.toBukkitPlayer(); // Returns the Bukkit Player

        ///////////////////////////////////////////////////////////////////////////////////
        PlayerMeta meta = aunaPlayer.getPlayerMeta(); // Gets the Player-Meta
        meta.set("key", "im a value"); // Sets a Meta Item
        String value = meta.getString("key2"); // Gets a Meta Item by Key
        aunaPlayer.updateMeta(); // Update´s the Meta (sending it to database)

        ///////////////////////////////////////////////////////////////////////////////////
        aunaPlayer.getCoins(); // Returns the Coins
        aunaPlayer.setSuffix("Suffix"); // Sets the Suffix to 'Suffix'
        aunaPlayer.sendActionbar("Im an Actionbar"); // Sends a Actionbar
        aunaPlayer.getMessageLanguage(); // Returns the Message Language
        aunaPlayer.getRank(); // Returns the PlayerRank
        aunaPlayer.isLoading(); // Returns if the Player is loaded

        /*
            NOTE: There are many more functions, but these are the Important ones
         */

    }

}
