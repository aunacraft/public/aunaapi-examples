import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.player.AunaPlayer;

import java.util.UUID;

public class CoinAPIExample {

    //-------------------------- Normal Coins --------------------------

    public void setCoins(UUID uuid, long coins) {
        AunaAPI.getApi().setCoins(uuid, coins); //Method is async
    }

    public void addCoins(UUID uuid, long coins) {
        AunaAPI.getApi().addCoins(uuid, coins); //Method is async
    }

    public void removeCoins(UUID uuid, long coins) {
        AunaAPI.getApi().removeCoins(uuid, coins); //Method is async
    }

    public long getCoins(UUID uuid) {
        return AunaAPI.getApi().getCoins(uuid);
    }

    public long getCoinsByPlayer(AunaPlayer player) {
        return player.getCoins();
    }

    //--------------------------Bank Coins Example --------------------------

    public void setBankCoins(UUID uuid, long coins) {
        AunaAPI.getApi().setBankCoins(uuid, coins); //Method is async
    }

    public void addBankCoins(UUID uuid, long coins) {
        AunaAPI.getApi().addBankCoins(uuid, coins); //Method is async
    }

    public void removeBankCoins(UUID uuid, long coins) {
        AunaAPI.getApi().removeBankCoins(uuid, coins); //Method is async
    }

    public long getBankCoins(UUID uuid) {
        return AunaAPI.getApi().getBankCoins(uuid);
    }

    public long getBankCoinsByPlayer(AunaPlayer player) {
        return player.getBankCoins();
    }
}
