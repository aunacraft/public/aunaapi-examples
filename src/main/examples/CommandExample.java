import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.commands.AunaCommand;
import net.aunacraft.api.commands.Command;
import net.aunacraft.api.commands.autocompleetion.impl.NoCompletionHandler;
import net.aunacraft.api.commands.autocompleetion.impl.PlayerCompletionHandler;
import net.aunacraft.api.commands.builder.CommandBuilder;
import net.aunacraft.api.commands.builder.ParameterBuilder;
import net.aunacraft.api.player.AunaPlayer;
import org.bukkit.entity.Player;

public class CommandExample implements AunaCommand {

    /*
        Method who is executed while startup
     */
    public void onEnable(){
        AunaAPI.getApi().registerCommand(new CommandExample()); // Registers the Command
    }

    /*
        Command-Name: commandname
        Permission: cmd.command, command.msg
        Usage: /commandname msg <Player> <Message>
     */

    @Override
    public Command createCommand() {
        return CommandBuilder.beginCommand("commandname") // Begins the Command Builder and sets the Command Name
                .aliases("cmdname", "cmd") // Adds aliases
                .permission("cmd.command") // Adds a permission to the whole command
                .description("I´m a Command description!") // Adds a description to the Command
                .subCommand(CommandBuilder.beginCommand("msg") // Begins a new Sub-Command named MSG;
                    .permission("command.msg") // Add´s a required permission to the Sub-Command
                        .parameter(ParameterBuilder.beginParameter("player") // Begins a new Parameter wich named 'player'
                                .required() // sets them required
                                .autoCompletionHandler(new PlayerCompletionHandler()) // Sets the AutoCompleteon-Handler to PlayerCompletion
                                .build() // builds the parameter
                        )
                        .parameter(ParameterBuilder.beginParameter("message") // Begins a new Parameter wich named 'message'
                                .required() // sets them required
                                .autoCompletionHandler(new NoCompletionHandler()) // Sets the AutoCompletion to NoCompletion
                                .openEnd() // Sets a non Limited Parameter (umlimited Arguments)
                                .build() // Builds the Parameter
                        )
                        .handler((aunaPlayer, commandContext, strings) -> { // adds a Command handler where you are able to handle command executions from the subcommand
                            AunaPlayer target = AunaAPI.getApi().getPlayer(commandContext.getParameterValue("player", Player.class).getName());
                            String message = commandContext.getParameterValue("message", String.class); // Gets the Parameter Value from 'message' parameter
                            // TODO YourTurn: Handle the Command
                        }).build()
                )
                .handler((aunaPlayer, commandContext, strings) -> {
                    // TODO YourTurn: handle non-subcommand use
                }).build();
    }
}
