import net.aunacraft.api.database.DatabaseHandler;
import net.aunacraft.api.database.config.impl.SpigotDatabaseConfig;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Consumer;

public class DatabaseHandlerExample {

    //-------------------------- DatabaseHandler Initialize --------------------------

    private final DatabaseHandler databaseHandler;

    public DatabaseHandlerExample(JavaPlugin plugin) {
        //DatabaseHandler need a DatabaseConfig
        //DatabaseConfig for Spigot-Plugins: SpigotDatabaseConfig
        //DatabaseConfig for BungeeCord-Plugins: BungeeCordDatabaseConfig
        this.databaseHandler = new DatabaseHandler(new SpigotDatabaseConfig(plugin));
    }

    //-------------------------- MySQL UPDATES --------------------------

    public void insertSomethingAsync() {
        this.databaseHandler
                .createBuilder("INSERT INTO example (exampleName, exampleValue) VALUES (?, ?)") //Create builder with sql statement
                    .addObjects("Cooler Name", "Coole Value") //set values (add objects)
                .updateAsync(() -> { //update async
                    //Run method is executed when the update is finished.
                    System.out.println("Update finished");
                });
    }

    public void insertSomethingSync() {
        this.databaseHandler
                .createBuilder("INSERT INTO example (exampleName, exampleValue) VALUES (?, ?)") //Create builder with sql statement
                .addObjects("Cooler Name", "Coole Value") //set values (add objects)
                .updateSync(); //update sync
    }

    //-------------------------- MySQL QUERIES --------------------------

    public void querySomethingAsync(Consumer<String> nameCallback) {
        this.databaseHandler
                .createBuilder("SELECT * FROM example WHERE exampleValue=?") //Create builder with sql statement
                .addObjects("Coole value") //set values (add objects)
                .queryAsync(rs -> { //query async
                    try {
                        if(rs.next()) { //check if resultset has next
                            String name = rs.getString("exampleName"); //get the name from result
                            nameCallback.accept(name); //accept the consumer with the name as value
                        }
                    }catch (SQLException e) {
                        e.printStackTrace();
                    }
                });
    }

    public String querySomethingSync() {
        ResultSet rs = this.databaseHandler
                .createBuilder("SELECT * FROM example WHERE exampleValue=?") //Create builder with sql statement
                .addObjects("Coole value") //set values (add objects)
                .querySync(); //query sync, returns a resultset
        try {
            if(rs.next()) { //check if resultset has next
                String name = rs.getString("exampleName"); //get the name from result
                return name; //return the name
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
