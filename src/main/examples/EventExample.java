import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.event.impl.BukkitPacketEvent;
import net.aunacraft.api.event.impl.PlayerRegisterEvent;
import net.aunacraft.api.event.impl.PlayerUnregisterEvent;
import net.aunacraft.api.player.AunaPlayer;
import net.minecraft.server.v1_16_R3.Packet;
import net.minecraft.server.v1_16_R3.PacketPlayInChat;

public class EventExample {

    /*
        Method who is executed while startup
     */
    public void onEnable(){
        AunaAPI.getApi().registerEventListener(PlayerRegisterEvent.class, EventExample::handleRegister); // Registers the event with a Static callable
        AunaAPI.getApi().registerEventListener(PlayerUnregisterEvent.class, this::handleUnregister); // Registers the event with a local callable
        AunaAPI.getApi().registerEventListener(BukkitPacketEvent.class, bukkitPacketEvent -> { // Registers the Event with normal lambda expression
            Packet<?> packet = bukkitPacketEvent.getPacket(); // Gets a Packet value
            AunaPlayer player = bukkitPacketEvent.getPlayer(); // Gets a Player value
            if(packet instanceof PacketPlayInChat){
                bukkitPacketEvent.setCanceled(true); // Cancels the Event
                player.toBukkitPlayer().sendMessage("Du darfst nicht in den Chat schreiben!");
            }
        });
    }

    public static void handleRegister(PlayerRegisterEvent event){
        AunaPlayer player = event.getPlayer();
        //TODO Handle the Event
    }

    public void handleUnregister(PlayerUnregisterEvent event){
        AunaPlayer player = event.getPlayer();
        //TODO Handle the Event
    }

}
