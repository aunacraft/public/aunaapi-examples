import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.gui.Gui;
import net.aunacraft.api.gui.GuiInventory;
import net.aunacraft.api.util.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

public class GuiExample extends Gui {

    public static void openInventory(Player player) { //Open inventory: this is normally done in other classes.
        new GuiExample(player) //create instance of the inventory
                .open(player); //open the inventory
    }

    public GuiExample(Player player) {
        super(
                AunaAPI.getApi().getMessageService().getMessageForPlayer(player, "example.gui.examplegui"), //inventory name -> get from message service
                9 * 3 //inventory-size;
        );
    }

    @Override
    public void initInventory(Player player, GuiInventory inventory) {
        inventory.fill(new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).setDisplayName("§c").build()); //fill inventory
        inventory.setItem(0, new ItemBuilder(Material.GREEN_BANNER).setDisplayName("§aNice banner").build()); //set item to slot 0
    }

    @Override
    public void onClick(Player player, InventoryClickEvent event) {
        int slot = event.getSlot();
        switch (slot) {
            case 0: //clicked slot is 0
                player.closeInventory();
                break;
            default: //other slots except 0
                player.sendMessage("No usage");
                break;
        }
    }
}
