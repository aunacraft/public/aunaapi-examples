import net.aunacraft.api.util.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

public class ItemBuilderExample {

    public void createItem() {
        ItemStack stack = new ItemBuilder(Material.STONE)
                .setDisplayName("Cooler stone")
                .setLore("Lore line 1", "lore line 2", "lore line 3", "lore line 4")
                .addEnchant(Enchantment.ARROW_DAMAGE, 0) //add enchantment with level 1
                .addItemFlags(ItemFlag.HIDE_UNBREAKABLE) //add flag to make the item unbreakable
                .build(); //build ItemStack
    }
}
