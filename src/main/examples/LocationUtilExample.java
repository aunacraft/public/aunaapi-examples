import net.aunacraft.api.bukkit.util.LocationUtil;
import org.bukkit.Location;
import org.bukkit.craftbukkit.libs.org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;

public class LocationUtilExample {

    private final File exampleFile = new File("exampleFile.json");

    public void writeLocationInFile(Location location) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(this.exampleFile);
            writer.write(LocationUtil.locationToString(location));
        }catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(writer != null) {
                IOUtils.closeQuietly(writer);
            }
        }
    }

    public Location getLocationFromFile() {
        try {
            String fileContent = new String(Files.readAllBytes(this.exampleFile.toPath()));
            return LocationUtil.locationFromString(fileContent);
        }catch (IOException e) {
            return null;
        }
    }

}
