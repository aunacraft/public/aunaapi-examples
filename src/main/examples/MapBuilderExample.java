import net.aunacraft.api.bukkit.maps.MapBuilder;
import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;
import org.bukkit.map.MinecraftFont;
import javax.imageio.ImageIO;
import java.io.IOException;
import java.net.URL;

public class MapBuilderExample {

    public void setMapToPlayers(){
        try {
            ItemStack item = new MapBuilder() //Initializing the MapBuilder class
                    .setRenderOnce(true) //Since this will be a static image, we only want it rendered once
                    .setImage(ImageIO.read(new URL("https://aunacraft.net/media/image.png"))) //Setting an image from a URL as background
                    .addText(0, 0, MinecraftFont.Font, "Hello there") //Adding some text with the Minecraft default font at 0, 0
                    .addCursor(20, 20, MapBuilder.CursorDirection.EAST, MapBuilder.CursorType.WHITE_DOT) //Adding a cursor (in our case a white dot) to the map
                    .build(); //Finally using the build method to generate an ItemStack

            Bukkit.getOnlinePlayers().forEach(player -> player.getInventory().addItem(item)); //Looping through all the online players and adding the ItemStack to their inventory
        } catch (IOException e) { //Exception thrown if the URL provided is invalid
            e.printStackTrace();
        }
    }

}
