import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.backend.message.MessageService;
import net.aunacraft.api.player.AunaPlayer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.bukkit.entity.Player;

import java.util.UUID;

public class MessageServiceExample {

    //-------------------------- Plugin Start stuff --------------------------

    //Example onEnable
    public void onEnable() {
        AunaAPI.getApi().getMessageService().applyPrefix("example", "example.prefix");
    }



    //-------------------------- Get Message by AunaPlayer --------------------------

    //the key in in the message file it looks like this: example.testmessage: "&cThis is a testmessage"
    public String getTranslatedMessage(AunaPlayer player, String key) {
        return player.getMessage(key);
    }

    //the key in in the message file it looks like this: example.testmessage.with_args: "&cThis is a testmessage with arg 1: {0} and arg 2: {1}"
    public String getTranslatedWithArguments(AunaPlayer player, String key, int testValue1, String testValue2) {
        return player.getMessage(key, testValue1, testValue2);
    }

    //-------------------------- Get Message by MessageService --------------------------

    //get message for bukkit player
    public String getTranslatedMessage(Player player, String key) {
        MessageService messageService = AunaAPI.getApi().getMessageService();
        return messageService.getMessageForPlayer(player, key);
    }

    //get message for bungeecord player
    public String getTranslatedMessage(ProxiedPlayer player, String key) {
        MessageService messageService = AunaAPI.getApi().getMessageService();
        return messageService.getMessageForProxiedPlayer(player, key);
    }

    //get message for uuid
    public String getTranslatedMessage(UUID uuid, String key) {
        MessageService messageService = AunaAPI.getApi().getMessageService();
        return messageService.getMessageForUUID(uuid, key);
    }
}
