import net.aunacraft.api.bukkit.npc.base.NPC;
import net.aunacraft.api.bukkit.npc.builder.NPCBuilder;
import net.aunacraft.api.bukkit.npc.event.NPCInteractEvent;
import net.aunacraft.api.bukkit.npc.event.NPCInteractListener;
import net.minecraft.server.v1_16_R3.PacketPlayInUseEntity;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class NPCExample implements NPCInteractListener { //Implements the interact listener

    public void createNPC(JavaPlugin plugin) {
        Location location = new Location(Bukkit.getWorld("world"), 0, 0, 0);
        new NPCBuilder(plugin) //create npc builder
                .setInteractListener(this) //set interact listener
                .setLocation(location) //set location of npc
                .setLookAtPlayer(true) //if true: npc looks at the player permanently
                .setNameKey("example.npc.example") //set namekey for npc (look at the MessageServiceExample)
                .build(); //build the npc: the npc is directly registered
    }

    @Override
    public void interact(NPCInteractEvent event) {
        NPC npc = event.getNpc(); //
        PacketPlayInUseEntity.EnumEntityUseAction action = event.getAction(); //get action from event
        Player player = event.getPlayer(); //get player from event
        if(action.equals(PacketPlayInUseEntity.EnumEntityUseAction.INTERACT))
            player.sendMessage("Du hast den npc " + npc.getNpcName() + " angeklickt mit der aktion: " + action.name());
    }
}
