import net.aunacraft.api.bukkit.signmenu.SignMenuFactory;
import net.aunacraft.api.util.validator.AunaValidators;
import org.bukkit.entity.Player;

public class SignMenuExample {

    public void openSignMenu(Player playerToOpen) {
        SignMenuFactory
                .newMenu( //create menu
                    "", //line 1
                    "--------------", //line 2
                    "Enter the amount", //line 3
                    "Example line")
                .response((player, lines) -> { //response
                   String numberAsString = lines[0];
                   if(AunaValidators.POSITIVE_INT_VALIDATOR.isValid(numberAsString)) {
                       int number = Integer.parseInt(numberAsString);
                       player.sendMessage("Du hast folgendes eingegeben: " + number);
                       return true;
                   }else {
                       player.sendMessage("§cEnter a valid number");
                       return false;
                   }
                })
                .open(playerToOpen); //open the sign menu for player
    }

}
