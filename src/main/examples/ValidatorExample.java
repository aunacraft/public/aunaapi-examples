import net.aunacraft.api.util.validator.AunaValidator;
import net.aunacraft.api.util.validator.AunaValidators;

public class ValidatorExample {

    //-------------------------- Validator from AunaValidators Class --------------------------

    public boolean isValidInteger(String s) {
        return AunaValidators.POSITIVE_INT_VALIDATOR.isValid(s);
    }


    //-------------------------- Use own Validator --------------------------

    public boolean isValidLong(String s) {
        return new ExampleValidator().isValid(s);
    }

    private class ExampleValidator implements AunaValidator {

        @Override
        public boolean isValid(String s) {
            try {
                long l = Long.parseLong(s);
                if(l > 10 && l < 20) { //check if the long is higher than 10 and smaller than 20 (11, 12, 13, 14, 15, 16, 17, 18, 19)
                    return true; //is valid
                }
            }catch (NumberFormatException e) {}
            return false; //is not valid
        }
    }

}
